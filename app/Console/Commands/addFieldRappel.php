<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Enrollment;
use App\CourseFile;
use Illuminate\Support\Carbon;
use App\Notifications\OneDayBeforeClass;
use Illuminate\Support\Facades\Mail;
use App\Mail\TomorrowNotifyEmail;

class addFieldRappel extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'rappel:coure-will-start-tomorrow';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Course will start tomorrow';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $cours = CourseFile::where("startDate", "<", Carbon::now()->addDays(1))
                ->where("startDate", ">", Carbon::now()->subDays(1))
                ->get();
        foreach($cours as $cour) {
            if($cour->is_notified == 0) {
                $enrollements= $cour->enrollments()->get();

                foreach($enrollements as $enrollement) {
                    $user = $enrollement->user()->get();
                    //$user[0]->notify(new OneDayBeforeClass($cour->startDate));
                    //Mail::to("sidmah10@gmail.com")->send(new OneDayBeforeClass($cour->startDate));
                    Mail::to($user[0]->email)->send(new TomorrowNotifyEmail());
                    Mail::to("sidmah10@gmail.com")->send(new TomorrowNotifyEmail());
                    $cour->update([
                        "is_notified" => 1
                    ]);
                }
            }
        }
         
        return 0;
    }
}
