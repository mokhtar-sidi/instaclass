<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Rating extends Model
{
    protected $fillable = [
        'user_id',
        'course_id',
        'teacher_id',
        'rate',
        'review',
        'is_approved',
    ];

    protected $casts = [
        'user_id' => 'integer',
        'course_id' => 'integer',
        'is_approved' => 'boolean',
    ];

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function teacher()
    {
        return $this->belongsTo('App\User');
    }

    public function file()
    {
        return $this->belongsTo('App\CourseFile', 'course_id', 'id');
    }
}
