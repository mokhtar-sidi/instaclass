<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Twilio\Jwt\AccessToken;
use Twilio\Jwt\Grants\VideoGrant;
use Twilio\Rest\Client;
use App\CourseFile;
use Storage;
use Illuminate\Support\Facades\Http;
use GuzzleHttp;
use function GuzzleHttp\Psr7\stream_for;
use GuzzleHttp\RequestOptions;

class LiveCoursesController extends Controller
{
    public function generate_token($myRoom, $user)
    {
        // Substitute your Twilio Account SID and API Key details
        $accountSid =  config('twilio.twilio_account_sid');
        $token = config("twilio.twilio_account_token"); 
        $apiKeySid = config("twilio.twilio_api_key");
        $apiKeySecret = config("twilio.twilio_api_secret");
        $identity = $user . "-" . uniqid();
        // Create an Access Token
        $token = new AccessToken(
            $accountSid,
            $apiKeySid,
            $apiKeySecret,
            3600,
            $identity
        );
        // Grant access to Video
        $grant = new VideoGrant();
        $grant->setRoom($myRoom);
        $token->addGrant($grant);
        // Serialize the token as a JWT
        return $token->toJWT();
    }

    public function createRoom($myRoom, $user, $recorded)
    {
        $sid =  config('twilio.twilio_account_sid');
        $token = config("twilio.twilio_account_token"); 
        $apiKeySid = config("twilio.twilio_api_key");
        $apiKeySecret = config("twilio.twilio_api_secret");
        /* $sid = env("TWILIO_ACCOUNT_SID");
        $token = env("TWILIO_ACCOUNT_TOKEN"); 
        $apiKeySid = env("TWILIO_API_KEY");
        $apiKeySecret = env("TWILIO_API_SECRET"); */
        /* $sid = "AC61cd66c66a7fab3539446cd5e8039719";
        $token = "d8f35d9eb86fe3595d650f3c4629664f";
        $apiKeySid = "SK7b71243558cb8fd34909e90e12e41f47";
        $apiKeySecret = "m5uuedPVotQHF9PNMB2mzKSoKzWjHeGV"; */
        info($sid);
        info($token);
        info($apiKeySid);
        info($apiKeySecret);
        $twilio = new Client($sid, $token);
        
        $room = $twilio->video->v1->rooms->create([
                    "recordParticipantsOnConnect" => $recorded,
                    "type" => "group",
                    "uniqueName" => $myRoom,
                ]);
        info('room created');
        // Grant access to Video
        $identity = "teacher-identity";
        // Create an Access Token
        $myToken = new AccessToken(
            $sid,
            $apiKeySid,
            $apiKeySecret,
            3600,
            $identity
        );
        info('AccessToken');
        $grant = new VideoGrant();
        $grant->setRoom($myRoom);
        $myToken->addGrant($grant);
        return response()->json([
            "sid" => $room->sid,
            "name" => $room->uniqueName,
            "token" => $myToken->toJWT(),
            "duration" => $room->duration]);
    }

    public function roomDetails($myRoom)
    {
        try {
            $sid =  config('twilio.twilio_account_sid');
            $token = config("twilio.twilio_account_token"); 
            $apiKeySid = config("twilio.twilio_api_key");
            $apiKeySecret = config("twilio.twilio_api_secret");

            $twilio = new Client($sid, $token);
            $room = $twilio->video->v1->rooms($myRoom)->fetch();
            Log::info("success");
            return response()->json([
                "name" => $room->uniqueName,
                "sid" => $room->sid,
                "status" => $room->status,
                "recorded" => $room->recordParticipantsOnConnect,
                "start" => $room->dateCreated,
                "end" => $room->endTime,
                "max_participants" => $room->maxParticipants,
                "duration" => $room->duration]);
        } catch (\Throwable $e) {
            Log::info("error");
            Log::info($e);
            return [];
        }
    }

    public function roomRecordings($roomSid)
    {
        $sid =  config('twilio.twilio_account_sid');
        $token = config("twilio.twilio_account_token"); 

        $twilio = new Client($sid, $token);

        $recordings = $twilio->video->v1->rooms($roomSid)
            ->recordings
            ->read([], 20);

//        foreach ($recordings as $record) {
        //            print($record->sid);
        //        }
        return response()->json($recordings);
    }

    public function roomParticipants($roomSid)
    {
        $sid =  config('twilio.twilio_account_sid');
        $token = config("twilio.twilio_account_token"); 

        $client = new Client($sid, $token);

        $participants = $client->video->rooms($roomSid)
            ->participants->read(array("status" => "connected"));
//        foreach ($participants as $participant) {
        //            echo $participant->identity;
        //        }
        return response()->json($participants);
    }

    public function closeRoom($myRoom, $roomsid, $course_id)
    {
        $sid =  config('twilio.twilio_account_sid');
        $token = config("twilio.twilio_account_token");

        $twilio = new Client($sid, $token);
        $room = $twilio->video->v1->rooms($myRoom)
            ->update("completed");

        $this->createRecording($roomsid, $course_id);
        return response()->json("Room completed");
    }

    public function removeParticipant($roomSid, $user)
    {
        $sid =  config('twilio.twilio_account_sid');
        $token = config("twilio.twilio_account_token"); 

        $client = new Client($sid, $token);
        $participant = $client->video->rooms($roomSid)
            ->participants($user)
            ->update(array("status" => "disconnected"));
        return response()->json(["status" => $participant->status, "user" => $participant->identity]);
    }

    public function myRooms($uniqueName)
    {
        $sid =  config('twilio.twilio_account_sid');
        $token = config("twilio.twilio_account_token");

        $twilio = new Client($sid, $token);
        $rooms = $twilio->video->v1->rooms->read(["uniqueName" => $uniqueName], 20);
//        echo $rooms;
        return response()->json($rooms);
    }

    public function retrieveRoomsbyStatus($status)
    {
        try {
            $sid =  config('twilio.twilio_account_sid');
            $token = config("twilio.twilio_account_token"); 

            $twilio = new Client($sid, $token);
            $rooms = $twilio->video->v1->rooms->read(["status" => $status], 20);
            $final_json = [];
            foreach ($rooms as $v) {
                $item['sid'] = $v->sid;
                $item['uniqueName'] = $v->uniqueName;
                $item['dateCreated'] = date_format($v->dateCreated, "Y-m-d H:i:s");
                $final_json[] = $item;
            }
            return response()->json($final_json);
        } catch (\Throwable $e) {
            Log::info("error" . $e);
            return [];
        }

    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function fetchRecordings()
    {
        $sid =  config('twilio.twilio_account_sid');
        $token = config("twilio.twilio_account_token"); 
        $apiKeySid = config("twilio.twilio_api_key");
        $apiKeySecret = config("twilio.twilio_api_secret");
 
        $twilio = new Client($sid, $token);
        
        $room_recording = $twilio->video->v1->rooms("RMa0478e02d49105ef4be40561b92cadee")
        ->recordings("RTc8d07ca47a164d3d2f2491a89827a1e0")
        ->fetch();

        $composition = $twilio->video->compositions->create('RMa0478e02d49105ef4be40561b92cadee', [
            'audioSources' => '*',
            'videoLayout' =>  array(
                                'transcode' => array (
                                    'video_sources' => array('*')
                                )
                              ),
            'statusCallback' => 'http://my.server.org/callbacks',
            'format' => 'mp4'
        ]);

        info($composition->format);
        info($composition->links);

        /* $roomSid = "RMa0478e02d49105ef4be40561b92cadee";
        $recordingSid = "RT4d13fd3227aaa98a5c950977daaefd0c";
        $uri = "https://video.twilio.com/v1/" .
            "Rooms/$roomSid/" .
            "Recordings/$recordingSid/" .
            "Media/";
        $response = $twilio->request("GET", $uri);
        $mediaLocation = $response->getContent()["redirect_to"];

        $media_content = file_get_contents($mediaLocation);
        print_r($media_content); */
        //$client = new Client($apiKeySid, $apiKeySecret);

        /* $recordings = $twilio->video->v1->rooms("RMa0478e02d49105ef4be40561b92cadee")
                                ->recordings
                                ->read([], 20);

        foreach ($recordings as $record) {
            info('recordings ids: ', [$record->sid]);
        } */


       /*  $sid = env('TWILIO_ACCOUNT_SID');
        $token = env('TWILIO_ACCOUNT_TOKEN');

        $client = new Client($sid, $token);

        $compositionHook = $client->video->compositionHooks->create(
            'MixingAllRoomAudiosHook', [
              'audioSources' => '*',
              'statusCallback' => 'http://my.server.org/callbacks',
              'format' => 'mp4'
        ]);
        info($compositionHook); */
        /* $compositionHook = $client->video->compositionHooks("HK149b8ee7fba9f98500af5326d853893b")->fetch();
        info($compositionHook->url);

        $compositionSid = "HK149b8ee7fba9f98500af5326d853893b";
        $uri = "https://video.twilio.com/v1/Compositions/HK149b8ee7fba9f98500af5326d853893b/Media?Ttl=3600";
        $response = $client->request("GET", $uri);
        info($response);
        $mediaLocation = $response->getContent();
        info($mediaLocation);

        // For example, download media to a local file
        file_put_contents("myFile.mp4", fopen($mediaLocation, 'r'));

        /* $recording = $twilio->video->v1->recordings("RTe9c773fdc0758abb91899a79070bf752")->fetch();

        $recordingSid = "RTe9c773fdc0758abb91899a79070bf752";
        $uri = "https://video.twilio.com/v1/Recordings/$recordingSid/Media";
        $response = $twilio->request("GET", $uri);
        $mediaLocation = $response->getContent()["redirect_to"];

        $media_content = file_get_contents($mediaLocation);
        info($mediaLocation);
        info($recording->url ."/Media"); */
        //file_get_contents($recording->url ."/Media");
        //return response()->download($media_content);
        return true;
    }

    public function createRecording(String $id, int $course_id)
    {
        $sid =  config('twilio.twilio_account_sid');
        $token = config("twilio.twilio_account_token"); 
        $apiKeySid = config("twilio.twilio_api_key");
        $apiKeySecret = config("twilio.twilio_api_secret");
 
        $twilio = new Client($sid, $token);

        $composition = $twilio->video->compositions->create($id, [
            'audioSources' => '*',
            'videoLayout' => array(
                                'transcode' => array (
                                    'video_sources' => array('*')
                                )
                            ),
            'statusCallback' => 'http://my.server.org/callbacks',
            'format' => 'mp4'
        ]);

        $file = CourseFile::where('id', $course_id)->first();
        info($composition->links['media']);
        $file->update([
            'record_link' => $composition->links['media'],
            'status' => 3,
            'is_finished' => 1,
        ]);

        return $file;

    }

    public function downloadRecordings(Request $request)
    {
        $sid =  config('twilio.twilio_account_sid');
        $token = config("twilio.twilio_account_token"); 
        $apiKeySid = config("twilio.twilio_api_key");
        $apiKeySecret = config("twilio.twilio_api_secret");

        $client = new Client($sid, $token);

        $response = $client->request("GET", $request['link']);
        $mediaLocation = $response->getContent()['redirect_to'];
        return $mediaLocation;
    } 
}
