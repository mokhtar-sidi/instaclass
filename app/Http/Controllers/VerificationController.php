<?php

namespace App\Http\Controllers;

use App\Notifications\VerifyEmail;
use App\User;
use App\RoleUser;
use Carbon\Carbon;
use Illuminate\Http\Request;

class VerificationController extends Controller
{
    public function verify($user_id, Request $request) {
        $user = User::find($user_id);
        $user->email_verified_at = Carbon::now();
        $user->save();
        $role = RoleUser::where('user_id', $user_id)->with('role')->first();

        if ($role->role->name == 'teacher')
            return redirect("/teacher/signin");
        else {
            return redirect("/auth/signin");
        }
    }

    public function resend($user_id, Request $request) {
        if (auth()->user()->hasVerifiedEmail()) {
            return response()->json(["msg" => "Email already verified."], 400);
        }
        $request->user()->notify(new VerifyEmail());
        return response()->json(["msg" => "Email verification link sent on your email id"]);
    }
}
