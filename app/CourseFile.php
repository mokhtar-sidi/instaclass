<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CourseFile extends Model
{
    protected $fillable = [
        'startDate',
        'timezone',
        'video_link',
        'is_notified',
        'record_link',
        'status',
        'is_finished',
    ];

    protected $casts = [
        'is_finished' => 'boolean',
        'is_notified' => 'boolean',
    ];

    public function enrollments()
    {
        return $this->hasMany(Enrollment::class);
    }

    public function course()
    {
        return $this->belongsTo('App\Course');
    }
}
