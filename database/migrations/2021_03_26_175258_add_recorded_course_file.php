<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddRecordedCourseFile extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('course_files', function (Blueprint $table) {
            $table->string('record_link')->nullable();
            $table->integer('status')->nullable();
            $table->boolean('is_finished')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('course_files', function (Blueprint $table) {
            $table->dropColumn('record_link');
            $table->dropColumn('status');
            $table->dropColumn('is_finished');
        });
    }
}
