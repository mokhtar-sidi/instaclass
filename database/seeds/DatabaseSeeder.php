<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $path = 'instaclass.sql';
        DB::unprepared(file_get_contents($path));
        $this->command->info('Country table seeded!');
           
        /* $this->call(RolesTableSeeder::class);
        $this->call(UsersTableSeeder::class); */
//        $this->call(CategoriesTableSeeder::class);
//        $this->call(SubCategorySeeder::class);
//        $this->call(CourseStatusSeeder::class);
//        $this->call(CoursesTableSeeder::class);
//        $this->call(SectionsSeeder::class);
//        $this->call(EnrollmentsSeeder::class);
//        $this->call(RatingsSeeder::class);
    }
}
