<?php

return [
    'twilio_account_sid' => env('TWILIO_ACCOUNT_SID', ''),
    'twilio_account_token' => env('TWILIO_ACCOUNT_TOKEN', ''),
    'twilio_api_key' => env('TWILIO_API_KEY', ''),
    'twilio_api_secret' => env('TWILIO_API_SECRET', ''),
];